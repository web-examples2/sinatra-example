ENV['CONSOLE_LEVEL'] ||= 'all'
TRACE_METHODS = true
LOG_DEPTH = 0
ENV['CONSOLE_OUTPUT'] = 'XTerm' # JSON,Text,XTerm,Default

SERVER_ENV = 'dev'
HTTP_HOST = '0.0.0.0'
HTTP_PORT = 8080
