require 'sinatra'
require 'sinatra/base'
require 'sinatra/namespace'
require 'sinatra/reloader'
require 'rack/sassc'
require 'slim'

PWD = File.dirname(__FILE__ )

Slim::Engine.set_default_options pretty: true

class SinatraSrv < Sinatra::Base
  register Sinatra::Namespace
  register Sinatra::Reloader
  enable :reloader

  use Rack::SassC, css_location: "#{PWD}/public/css", scss_location: "#{PWD}/public/css", create_map_file: true, syntax: :sass, check: true

  set :root, PWD

  error do
    "<h1>Error</h1>"
  end

  get('/') do
    slim :index
  end

  not_found { '404 page' }

  # get('*') { slim request.path_info.to_sym }

end


