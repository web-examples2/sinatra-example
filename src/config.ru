#!/usr/bin/env ruby
require 'async'
require_relative 'config.rb'
require_relative 'logging'
require_relative 'server/server'
require 'rack/handler/falcon'

Async do
  Rack::Handler::Falcon.run SinatraSrv, Port: HTTP_PORT, Host: HTTP_HOST
end