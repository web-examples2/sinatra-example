require 'console'
require 'fiber'

# https://socketry.github.io/traces/guides/getting-started/index.html
# OpenTelemetry / Datadog
# https://socketry.github.io/console/guides/getting-started/index.html
# ENV['TRACES_BACKEND'] = 'traces/backend/console'
# ENV['CONSOLE_LEVEL'] = 'all'
# ENV['CONSOLE_OUTPUT'] = 'XTerm' # JSON,Text,XTerm,Default
# TRACE_METHODS = true
TRACE_METHODS ||= true unless defined? TRACE_METHODS
if TRACE_METHODS
  trace = TracePoint.new(:call, :return, :b_call, :b_return) { |tp| # :thread_begin, :thread_end
    call_stack = Thread.current[:call_stack] ||= {}
    call_stack_fiber = call_stack[Fiber.current.__id__] ||=[]
    call_stack_fiber << [tp.defined_class, tp.method_id] if [:call, :b_call].include? tp.event
    call_stack_fiber.pop if [:return, :b_return].include? tp.event
  }
  trace.enable
end
# the_method
# trace.disable
LOG_DEPTH ||= 10 unless defined? LOG_DEPTH
LOGGER = Class.new {
  def method_missing(name, *args)
    debug_level = name[/(\d+)/,1].to_i
    unless debug_level > LOG_DEPTH
      if TRACE_METHODS
        call_stack = Thread.current[:call_stack] ||= {}
        call_stack_fiber = call_stack[Fiber.current.__id__] ||=[]
        caller = call_stack_fiber[-2] ? call_stack_fiber[-2].join('.').gsub('Class:','').gsub(/[#<>]/,'') : ''
        msg = "\e[33m#{caller}:\e[0m \e[38;5;254m" + args.flatten.map(&:inspect).join(', ')
      else
        msg = args.flatten.map(&:inspect).join(', ')
      end

      Console.logger.send  name.to_s.gsub(/\d/,''), msg
    end
  end
}.new

LOGGER_GRAPE = Class.new {
  def method_missing(name, d)
    Console.logger.send  name, "REST_API: #{d[:method]} #{d[:path]} #{d[:params]} - #{d[:status]} host:#{d[:host]} time:#{d[:time]}"
  end
}.new